//
//  Contants.swift
//  Vox
//
//  Created by Jayesh Mardiya on 06/09/18.
//  Copyright © 2018 Jayesh Mardiya. All rights reserved.
//

import Foundation
import UIKit

let appDelegate         = UIApplication.shared.delegate as! AppDelegate
let mainStoryboard     = UIStoryboard(name: "Main", bundle: nil)
let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
