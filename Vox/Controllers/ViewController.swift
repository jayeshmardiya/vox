//
//  ViewController.swift
//  Vox
//
//  Created by Jayesh Mardiya on 06/09/18.
//  Copyright © 2018 Jayesh Mardiya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK:- Declarations -
    
    @IBOutlet weak var viewLogo: UIView!
    @IBOutlet weak var btnTC: UIButton!
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    
    // MARK:- ViewController Life Cycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        viewLogo.setCornerRadius(cornerRadius: 20, borderWidth: 0.8, color: .lightGray)
        btnDecline.setCornerRadius(cornerRadius: 25, borderWidth: 0.0, color: .clear)
        btnAccept.setCornerRadius(cornerRadius: 25, borderWidth: 0.0, color: .clear)
    }

    // MARK:- Memory Warnings -
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Action Methods -
    
    @IBAction func btnTCClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
        
        if self.btnTC.isSelected == true && self.btnPrivacyPolicy.isSelected == true {
            self.btnAccept.backgroundColor = UIColor.init(red: 255.0/255.0, green: 45.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        } else {
            self.btnAccept.backgroundColor = UIColor.groupTableViewBackground
        }
    }
    
    @IBAction func btnPrivacyPolicyClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
        
        if self.btnTC.isSelected == true && self.btnPrivacyPolicy.isSelected == true {
            self.btnAccept.backgroundColor = UIColor.init(red: 255.0/255.0, green: 45.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        } else {
            self.btnAccept.backgroundColor = UIColor.groupTableViewBackground
        }
    }
    
    @IBAction func btnDeclineClicked(_ sender: UIButton) {
        
        exit(0)
    }
    
    @IBAction func btnAcceptClicked(_ sender: UIButton) {
        
        if self.btnTC.isSelected == true && self.btnPrivacyPolicy.isSelected == true {
            
            UserDefaults.standard.set(true, forKey: "TERMS_ACCEPTED")
            appDelegate.goToHomeView()
        }
    }
    
    // MARK:- Custom Methods -
}
